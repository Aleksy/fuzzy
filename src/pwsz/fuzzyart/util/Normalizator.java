package pwsz.fuzzyart.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasa normalizatora
 */
public class Normalizator {
   /**
    * Metoda normalizująca wektor o długości N do postaci wektora o długości 2N złożonego z dwóch wektorów komplanarnych.
    *
    * @param vector wektor wejściowy
    * @return wektor znormalizowany
    */
   public static List<Double> complanary(List<Double> vector) {
      List<Double> normalized = new ArrayList<>(vector);
      for (Double v : vector) {
         normalized.add(1 - v);
      }
      return normalized;
   }

   /**
    * Metoda normalizująca wektor w taki sposób, że wartość największa zostaje zamieniona w 1.0, a reszta wyzerowana.
    *
    * @param vector wektor wejściowy
    * @return wektor znormalizowany
    */
   public static List<Double> hotOne(List<Double> vector) {
      List<Double> normalized = new ArrayList<>();
      int index = 0;
      double max = vector.get(index);
      for (int i = 0; i < vector.size(); i++) {
         Double v = vector.get(i);
         if(v > max) {
            max = v;
            index = i;
         }
      }
      for (int i = 0; i < vector.size(); i++) {
         normalized.add(i == index ? 1.0 : 0.0);
      }
      return normalized;
   }
}
