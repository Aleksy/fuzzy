package pwsz.fuzzyart.util;

import pwsz.fuzzyart.model.neural.Neuron;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Klasa użytkowa dla sieci Fuzzy Art
 */
public class FuzzyArtUtil {
   /**
    * Metoda tworzy nową warstwę dla sieci neuronowej.
    * @param layerSize rozmiar warstwy
    * @param inputSize rozmiar wektora wejściowego (liczba wag każdego neuronu)
    * @param randomizeWeights wartość boolean określająca czy wagi powinny być losowe
    * @return
    */
   public static List<Neuron> createLayer(int layerSize, int inputSize, boolean randomizeWeights) {
      List<Neuron> neurons = new ArrayList<>();
      for(int i = 0; i < layerSize; i++) {
         neurons.add(new Neuron(createWeights(inputSize, randomizeWeights)));
      }
      return neurons;
   }

   /**
    * Metoda tworzy wagi. W przypadku wybrania wag losowych uwzględniana jest wielkość
    * wektora wejściowego. Oznacza to, że dla neuronu o 40 wejściach (40 wagach) dla każdej
    * wagi zostaje wylosowana liczba z przedziału od 0.0 - 1.0, która następnie podzielona jest przez 40.
    * @param size rozmiar wektora wag
    * @param randomizeWeights wartość boolean określająca czy wagi powinny być losowe
    * @return nowy zestaw wag
    */
   public static List<Double> createWeights(int size, boolean randomizeWeights) {
      List<Double> weights = new ArrayList<>();
      Random random = new Random();
      for (int i = 0; i < size; i++) {
         weights.add(randomizeWeights ? (random.nextDouble() / size) : 1.0);
      }
      return weights;
   }
}
