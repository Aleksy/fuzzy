package pwsz.fuzzyart.model.neural;

import java.util.ArrayList;
import java.util.List;

/**
 * Model neuronu
 */
public class Neuron {
   /**
    * Wektor wag neuronu
    */
   private List<Double> weights;

   /**
    * Konstruktor
    * @param weights wektor wag
    */
   public Neuron(List<Double> weights) {
      this.weights = weights;
   }

   /**
    * Główna funkcja neuronu
    *
    * @param inputs do przetworzenia
    * @return wyjście neuronu
    */
   Double f(List<Double> inputs) {
      Double sum = 0.0;
      for (int i = 0; i < inputs.size(); i++) {
         sum += weights.get(i) * inputs.get(i);
      }
      return sum;
   }

   /**
    * Getter dla parametru weights
    *
    * @return weights
    */
   List<Double> getWeights() {
      return weights;
   }

   /**
    * Metoda dodająca nową wagę do neuronu
    */
   void addWeight() {
      weights.add(1.0);
   }

   /**
    * Metoda korygująca wagę neuronu
    * @param index miejsce znajdowania się starej wagi
    * @param value nowa wartość wagi
    */
   void correctWeight(int index, double value) {
      List<Double> newWeights = new ArrayList<>();
      for (int i = 0; i < weights.size(); i++) {
         if(i == index) {
            newWeights.add(value);
         } else {
            newWeights.add(weights.get(i));
         }
      }
      this.weights = newWeights;
   }
}
