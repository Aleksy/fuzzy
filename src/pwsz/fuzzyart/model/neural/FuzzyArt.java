package pwsz.fuzzyart.model.neural;

import pwsz.fuzzyart.constants.Parameters;
import pwsz.fuzzyart.util.FuzzyArtUtil;
import pwsz.fuzzyart.util.Normalizator;

import java.util.ArrayList;
import java.util.List;

/**
 * Model sieci Fuzzy ART
 */
public class FuzzyArt {
   /**
    * Warstwa wejściowa
    */
   private List<Neuron> inputLayer;
   /**
    * Warstwa wyjściowa
    */
   private List<Neuron> outputLayer;

   /**
    * Konstruktor
    *
    * @param inputSize       rozmiar wektora wejściowego (połowa rozmiaru warstwy wejściowej, konstruktor
    *                        uwzględnia to, że w sieci Fuzzy ART wektor wejściowy jest powiększany
    *                        o wektor komplanarny)
    * @param outputLayerSize rozmiar warstwy wyjściowej
    */
   public FuzzyArt(int inputSize, int outputLayerSize) {
      inputLayer = FuzzyArtUtil.createLayer(2 * inputSize, outputLayerSize, false);
      outputLayer = FuzzyArtUtil.createLayer(outputLayerSize, 2 * inputSize, true);
   }

   /**
    * Główna funkcja obliczająca sygnał wyjściowy. W funkcji tej wektor wejściowy jest odpowiednio przygotowany:
    * dodaje się do niego wektor komplanarny. Wektor wyjściowy poddany jest operacji "hot one".
    *
    * @param input wektor wejściowy
    * @return wektor wyjściowy
    */
   public List<Double> predict(List<Double> input) {
      List<Double> normalizedInput = Normalizator.complanary(input);
      List<Double> output = new ArrayList<>();
      for (Neuron neuron : outputLayer) {
         output.add(neuron.f(normalizedInput));
      }
      return Normalizator.hotOne(output);
   }

   /**
    * Główna funkcja pokazująca modelowi zadany wektor i dopasowująca go do niego.
    * Funkcja ta przygotowuje wektor wejściowy uzupełniając go o wektor komplanarny.
    * Następnie odnajdywany jest neuron zwycięski, który w zależności od tego czy jest
    * wystarczająco podobny do wzorca - jest uczony, bądź tworzony jest nowy neuron.
    * W tym drugim przypadku następuje rekurencja i uczenie przy wybranym wektorze wejściowym
    * przebiega od początku, z nowododanym neuronem.
    *
    * @param input wektor wejściowy
    */
   public void fit(List<Double> input) {
      List<Double> normalizedInput = Normalizator.complanary(input);
      Neuron winner = choiceFunction(normalizedInput);
      double similarity = matchFunction(winner, normalizedInput);
      if (similarity < Parameters.SIMILARITY_RATE) {
         addNewNeuron(normalizedInput.size());
         fit(input);
      } else {
         layersCorrection(winner, normalizedInput);
      }
   }

   private void layersCorrection(Neuron winner, List<Double> normalizedInput) {
      int winnerIndex = outputLayer.indexOf(winner);
      // modyfikacja STM (Short Time Memory)
      for (int j = 0; j < inputLayer.size(); j++) {
         Double inputWeight = inputLayer.get(j).getWeights().get(winnerIndex);
         double inputCorrected = Parameters.LEARNING_RATE * fuzzyAnd(inputWeight, normalizedInput.get(j))
            + (1 - Parameters.LEARNING_RATE) * inputWeight;
         inputLayer.get(j).correctWeight(winnerIndex, inputCorrected);
      }
      // modyfikacja LTM (Long Time Memory)
      for (int i = 0; i < winner.getWeights().size(); i++) {
         Double inputWeight = inputLayer.get(i).getWeights().get(winnerIndex);
         double sum = 0.0;
         for(int j = 0; j < inputLayer.size(); j++) {
            sum += fuzzyAnd(inputLayer.get(i).getWeights().get(winnerIndex), normalizedInput.get(j));
         }
         double outputCorrected = inputWeight / (0.5 + sum);
         winner.correctWeight(i, outputCorrected);
      }
   }

   private void addNewNeuron(int inputSize) {
      // dodanie nowego neuronu, a także powiększenie liczby wag w neuronach poprzedniej warstwy
      outputLayer.add(new Neuron(FuzzyArtUtil.createWeights(inputSize, true)));
      for (Neuron neuron : inputLayer) {
         neuron.addWeight();
      }
   }

   /**
    * Funkcja obliczająca jak bardzo neuron jest podobny do zaprezentowanego wzorca. Jest to
    * obliczane przy wykorzystaniu operacji fuzzy AND (AND w logice rozmytej)
    * @param neuron neuron zwycięski
    * @param input wektor wejściowy
    * @return miara podobieństwa
    */
   private double matchFunction(Neuron neuron, List<Double> input) {
      double winnerSum = 0.0;
      double inputSum = 0.0;
      for (int i = 0; i < neuron.getWeights().size(); i++) {
         Double w = neuron.getWeights().get(i);
         Double x = input.get(i);
         winnerSum += fuzzyAnd(x, w);
         inputSum += x;
      }
      return winnerSum / inputSum;
   }

   /**
    * Funkcja wybierająca neuron zwycięski przy użyciu logiki rozmytej.
    * @param input wektor wejściowy
    * @return zwycięski neuron
    */
   private Neuron choiceFunction(List<Double> input) {
      Neuron winner = outputLayer.get(0);
      double maxSum = Double.MIN_VALUE;
      for (int i = 0; i < outputLayer.size(); i++) {
         Neuron neuron = outputLayer.get(i);
         double sum = 0.0;
         for (int j = 0; j < neuron.getWeights().size(); j++) {
            double x = input.get(j);
            double w = neuron.getWeights().get(j);
            sum += fuzzyAnd(x, w);
         }
         if (sum > maxSum) {
            maxSum = sum;
            winner = neuron;
         }
      }
      return winner;
   }

   /**
    * Rozmyta bramka AND
    * @param x1 x1
    * @param x2 x2
    * @return AND(x1, x2)
    */
   private double fuzzyAnd(double x1, double x2) {
      return Math.min(x1, x2);
   }

   @Override
   public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      for (Neuron neuron : outputLayer) {
         stringBuilder.append("<");
         for (Double weight : neuron.getWeights()) {
            stringBuilder.append(round(weight)).append(" ");
         }
         stringBuilder.append(">");
      }
      stringBuilder.append("\n");
      for (Neuron neuron : inputLayer) {
         stringBuilder.append("<");
         for (Double weight : neuron.getWeights()) {
            stringBuilder.append(round(weight)).append(" ");
         }
         stringBuilder.append(">");
      }

      return stringBuilder.toString();
   }

   private String round(double x) {
      x *= 100;
      x = Math.round(x);
      x /= 100;
      return Double.toString(x);
   }
}
