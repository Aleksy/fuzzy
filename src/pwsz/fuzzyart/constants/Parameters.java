package pwsz.fuzzyart.constants;

/**
 * Parametry
 */
public class Parameters {
   /**
    * Współczynnik uczenia, wykorzystywany do "wysubtelnienia" procesu uczenia
    */
   public static final double LEARNING_RATE = 0.1;
   /**
    * Współczynnik podobieństwa, określa stopień podobieństwa, od którego trening neuronu zwycięskiego jest podejmowany.
    * Im mniejsza wartość tym mniej nowych neuronów zostaje stworzonych (czyli im mniejsza wartość tym większa
    * generalizacja kategorii). Większa wartość oznacza większe uszczegółowienie i większą precyzję w kategoryzowaniu.
    */
   public static final double SIMILARITY_RATE = 0.0001;
   /**
    * Liczba iteracji procesu uczenia
    */
   public static final int ITERATIONS = 10000;
}
