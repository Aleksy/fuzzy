package pwsz.fuzzyart;

import pwsz.fuzzyart.constants.Parameters;
import pwsz.fuzzyart.model.neural.FuzzyArt;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Główna funkcja programu
 */
public class Main {

   private static List<List<Double>> inputs;

   private static List<List<Double>> init() throws IOException {
      List<List<Double>> inputs = new ArrayList<>();
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0))); // st. art
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0))); // st. wf
//      inputs.add(new ArrayList<>(Arrays.asList(0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0))); // st. hum
//      inputs.add(new ArrayList<>(Arrays.asList(0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0))); // st. ścisłe
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 0.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(0.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 1.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0)));
//      inputs.add(new ArrayList<>(Arrays.asList(1.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 0.0)));
      BufferedImage image = ImageIO.read(new File("inputs/image.png"));
      for (int y = 0; y < image.getHeight(); y++) {
         List<Double> input = new ArrayList<>();
         for (int x = 0; x < image.getWidth(); x++) {
            Color color = new Color(image.getRGB(x, y));
            double v = (color.getRed() / 255.0 + color.getGreen() / 255.0 + color.getBlue() / 255.0) / 3.0;
            input.add(v);
         }
         inputs.add(input);
      }
      return inputs;
   }

   /**
    * Główna metoda programu
    * @param args argumenty
    * @throws IOException wyjątek we-wy
    */
   public static void main(String[] args) throws IOException {
      inputs = init();
      FuzzyArt fuzzyArt = new FuzzyArt(inputs.get(0).size(), inputs.get(0).size());
      Random random = new Random();
      for (int i = 0; i < Parameters.ITERATIONS; i++) {
         fuzzyArt.fit(inputs.get(random.nextInt(inputs.size())));
      }
      for (List<Double> input : inputs) {
         List<Double> predict = fuzzyArt.predict(input);
         for (int i = 0; i < predict.size(); i++) {
            if (predict.get(i) > 0.9) {
               System.out.print(i);
            }
         }
         System.out.println();
      }
   }
}
